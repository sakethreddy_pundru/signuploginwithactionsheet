//
//  SignupViewController.m
//  Task1
//
//  Created by admin on 17/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "SignupViewController.h"

@interface SignupViewController ()

@end

@implementation SignupViewController{
    UITextField *username;
    
    UITextField *password;
    
    UITextField *reEnterPassword;
    
    UIButton *signup;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIImage *pic = [UIImage imageNamed:@"Aerial01.jpg"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:pic];
    [self.view addSubview:imageView];

    
    username = [[UITextField alloc]initWithFrame:CGRectMake(75, 120, 200, 50)];
    username.textColor = [UIColor blackColor];
    username.borderStyle = UITextBorderStyleRoundedRect;
    username.backgroundColor= [UIColor lightTextColor];
    username.placeholder = @"Username";
    [self.view addSubview:username];
    
    password = [[UITextField alloc]initWithFrame:CGRectMake(75, 200, 200, 50)];
    password.textColor = [UIColor blackColor];
    password.borderStyle = UITextBorderStyleRoundedRect;
    password.backgroundColor= [UIColor lightTextColor];
    password.placeholder = @"Password";
    password.secureTextEntry = YES;
    [self.view addSubview:password];
    
    reEnterPassword = [[UITextField alloc]initWithFrame:CGRectMake(75, 280, 200, 50)];
    reEnterPassword.textColor = [UIColor blackColor];
    reEnterPassword.borderStyle = UITextBorderStyleRoundedRect;
    reEnterPassword.backgroundColor= [UIColor lightTextColor];
    reEnterPassword.placeholder = @"reEnterPassword";
    reEnterPassword.secureTextEntry = YES;
    [self.view addSubview:reEnterPassword];
    
    signup = [[UIButton alloc]initWithFrame:CGRectMake(100, 350, 150, 150)];
    [signup setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [signup setTitle:@"signup" forState:UIControlStateNormal];
    [signup addTarget:self action:@selector(performSignup) forControlEvents:UIControlEventTouchUpInside];
    signup.backgroundColor = [UIColor blueColor];
    signup.frame = CGRectMake(50, 370, 125, 50);
    signup.layer.cornerRadius=30;
    [self.view addSubview:signup];

    
}
-(void)performSignup{
    [[NSUserDefaults standardUserDefaults]setValue:username.text forKey:@"Username"];
    [[NSUserDefaults standardUserDefaults]setValue:password.text forKey:@"Password"];
    [[NSUserDefaults standardUserDefaults]synchronize];
    
    if ([password.text isEqualToString:reEnterPassword.text]){
        NSLog(@"password matched");
        
        UIAlertView *success = [[UIAlertView alloc]initWithTitle:@"hiiii" message:@"your successfully registered " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [success show];
        
        
    }
    
    else {
        
        UIAlertView *error = [[UIAlertView alloc]initWithTitle:@"oooops" message:@"passwords mismatched " delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [error show];
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
