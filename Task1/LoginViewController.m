//
//  LoginViewController.m
//  Task1
//
//  Created by admin on 17/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "LoginViewController.h"
#import "SuccessViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController{
    UITextField *username;
    
    UITextField *password;
    
    
    UIButton *login;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    UIImage *pic = [UIImage imageNamed:@"Aerial01.jpg"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:pic];
    [self.view addSubview:imageView];

    
    username = [[UITextField alloc]initWithFrame:CGRectMake(75, 120, 200, 50)];
    username.textColor = [UIColor blackColor];
    username.borderStyle = UITextBorderStyleRoundedRect;
    username.backgroundColor= [UIColor lightTextColor];
    username.placeholder = @"Username";
    [self.view addSubview:username];
    
    password = [[UITextField alloc]initWithFrame:CGRectMake(75, 200, 200, 50)];
    password.textColor = [UIColor blackColor];
    password.borderStyle = UITextBorderStyleRoundedRect;
    password.backgroundColor= [UIColor lightTextColor];
    password.placeholder = @"Password";
    password.secureTextEntry = YES;
    [self.view addSubview:password];
    
    login = [[UIButton alloc]initWithFrame:CGRectMake(200, 350, 150, 150)];
    [login setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [login setTitle:@"Login" forState:UIControlStateNormal];
    [login addTarget:self action:@selector(performlogin:)forControlEvents:UIControlEventTouchUpInside];
    login.backgroundColor = [UIColor blueColor];
    login.frame=CGRectMake(190, 370, 125, 50);
    login.layer.cornerRadius = 25;
    [self.view addSubview:login];
    // Do any additional setup after loading the view.
}


-(void)performlogin:(id)sender{
    
    NSString *usernametf;
    NSString *passwordtf;
    usernametf = [[NSUserDefaults standardUserDefaults]valueForKey:@"Username"];
    passwordtf = [[NSUserDefaults standardUserDefaults]valueForKey:@"Password"];
    if ([username.text isEqualToString:usernametf]&&[password.text isEqualToString:passwordtf]){
        
        SuccessViewController *success = [[SuccessViewController alloc]init];
        [self.navigationController pushViewController:success animated:YES];
        
    }
    
    else {
        
        UIAlertView *note2 = [[UIAlertView alloc]initWithTitle:@"oops" message:@"invalid credentials" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
        [note2 show];
        
        
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
