//
//  SuccessViewController.m
//  Task1
//
//  Created by admin on 17/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "SuccessViewController.h"

@interface SuccessViewController ()

@end

@implementation SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIImage *pic = [UIImage imageNamed:@"NatGeo12.jpg"];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:pic];
    [self.view addSubview:imageView];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(100,0,240,200)];
    label.text = @"hiii WELCOME";
    label.textColor=[UIColor whiteColor];
    [self.view addSubview:label];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
