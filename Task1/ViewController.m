//
//  ViewController.m
//  Task1
//
//  Created by admin on 17/10/1937 SAKA.
//  Copyright (c) 1937 SAKA admin. All rights reserved.
//

#import "ViewController.h"
#import "SignupViewController.h"
#import "LoginViewController.h"

@interface ViewController ()

@end

@implementation ViewController{
    
    }



- (void)viewDidLoad {
    [super viewDidLoad];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(100, 200, 200, 100)];
    button.backgroundColor = [UIColor whiteColor];
    [button setTitle:@"Attach" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonClicked)forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)buttonClicked{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Alert!!!" message:@"Select:" preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *action1 = [UIAlertAction actionWithTitle:@"Signup" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        SignupViewController *svc = [[SignupViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
                
        
    }];
    
    UIAlertAction *action2 = [UIAlertAction actionWithTitle:@"Login" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        
        
        LoginViewController *svc = [[LoginViewController alloc]init];
        [self.navigationController pushViewController:svc animated:YES];
        
    }];
    
    UIAlertAction *action3 = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
    }];
    
    [alert addAction:action1];
    [alert addAction:action2];
    [alert addAction:action3];
    
    
    [self presentViewController: alert animated:YES completion:^{
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
